package com.epam.courses;

import com.epam.courses.view.View;

public class Main {
  public static void main(String[] args) {
    new View("LanguageView").show();
  }
}
