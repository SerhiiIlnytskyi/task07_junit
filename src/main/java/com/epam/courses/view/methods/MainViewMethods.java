package com.epam.courses.view.methods;

import com.epam.courses.controller.Controller;
import com.epam.courses.view.View;
import java.util.LinkedHashMap;
import java.util.Map;

public class MainViewMethods implements ViewMethods{

  private Controller controller;
  private Map<String, Executable> methodsMenu;

  public MainViewMethods() {
    this.controller = new Controller();
    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::button);
    methodsMenu.put("L", this::languageMenu);
    //todo add another methods
  }
  private void button() {
    controller.todo();
  }

  private void languageMenu() {
    new View("LanguageView").show();
  }

  public Map<String, Executable> getMethodsMenu() {
    return methodsMenu;
  }

  public void setMethodsMenu(Map<String, Executable> methodsMenu) {
    this.methodsMenu = methodsMenu;
  }

}
